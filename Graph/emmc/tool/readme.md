# nand-sata-install
Doc:
https://docs.armbian.com/User-Guide_Getting-Started/#how-to-install-to-emmc-nand-sata-usb

Discussion: https://forum.armbian.com/topic/12983-workaround-s905x3-retaining-android-yet-booting-armbian-from-sd-card/?tab=comments#comment-95007

Guide, keep:
https://forum.armbian.com/topic/7875-how-nand-sata-install-works/

src: https://github.com/armbian/build/blob/master/packages/bsp/common/usr/sbin/nand-sata-install
