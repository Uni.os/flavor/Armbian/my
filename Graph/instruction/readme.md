by: Armin
https://forum.armbian.com/topic/12988-armbian-for-amlogic-s905x3/page/7/?tab=comments#comment-101721

[Posted May 17](https://forum.armbian.com/topic/12988-armbian-for-amlogic-s905x3/page/7/?tab=comments#comment-101789)
I have shutdown the system, remove the power supply, remove the SD Card and reconnect the power supply.
But unfortunately, the system doesn't want to boot from eMMC (no bootloop as previously, the TV can detect HDMI signal but blackscreen).
I'm lost !


[Posted May 17 (edited)](https://forum.armbian.com/topic/12988-armbian-for-amlogic-s905x3/page/7/?tab=comments#comment-101790)
Seems to be the same issue here: https://forum.armbian.com/topic/7930-armbian-for-amlogic-s9xxx-kernel-5x/page/53/?tab=comments#comment-98285
I'm waiting 15min without success. I wil try otherd DTB but you have any idea to increase my chances !
Maybe, @root-stas will have solution later: https://forum.armbian.com/topic/12162-single-armbian-image-for-rk-aml-aw/page/26/?tab=comments#comment-101783


[almotra, Posted March 31](https://forum.armbian.com/topic/12988-armbian-for-amlogic-s905x3/page/5/?tab=comments#comment-98176)
I answer myself.
After booting on the sd card, copy and rename the u-boot.ext file to u-boot.sd with the commands:
cd /boot
then
cp u-boot.ext u-boot.sd
then
cd
then
/root/install-aml.sh
