repo:
https://github.com/75h29hf65/Build-Armbian.git

commit:
commit b60fa9b91d06552d3f10b0ff9ab79fbdec27ee97 (grafted, HEAD -> master, origin/master, origin/HEAD)
Author: 75h29hf65 <uryity84h5@outlook.com>

log:
[ o.k. ] SHA256 calculating [ Armbian_20.09_Arm-64_focal_current_5.8.10.img ]
[ warn ] GPG signing skipped - no GPG_PASS [ Armbian_20.09_Arm-64_focal_current_5.8.10.img ]
[ .... ] Fingerprinting
[ o.k. ] Done building [ /home/vagrant/armbian/output/images/Armbian_20.09_Arm-64_focal_current_5.8.10.img ]
[ o.k. ] Runtime [ 14 min ]
[ o.k. ] Repeat Build Options [ ./compile.sh  BOARD=arm-64 BRANCH=current RELEASE=focal BUILD_MINIMAL=no BUILD_DESKTOP=no KERNEL_ONLY=no KERNEL_CONFIGURE=no COMPRESS_OUTPUTIMAGE=sha,gpg,img  ]

dtb:
- I used /dtb/amlogic/meson-sm1-sei610.dtb already on the box.
